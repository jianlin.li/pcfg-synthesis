import torch
import tqdm
import torch as T
import json
import pyro
import time
import argparse
import numpy
from datetime import datetime

from pathlib import Path

from pcfg_gpsyn_guide import *
from pcfg_gpsyn_model import *
from TraceWrapper import *
from pcfg_gpsyn_utils import *

T.set_default_dtype(T.float64)

class synthesizer:
    def __init__(
            self,
            name,
            xs,
            ys,
            lr=0.005,
            n_steps=10,
            num_inference_samples=100,
            training_batch_size=32,
            MAX_DEPTH=5,
            hidden_flow_size=35,
            num_hidden_units=35
    ):
        self.name = name
        self.lr = lr
        self.n_steps = n_steps
        self.num_inference_samples = num_inference_samples
        self.training_batch_size = training_batch_size
        self.xs = xs
        self.ys = ys
        self.n = xs.size(dim=0)
        self.MAX_DEPTH  = MAX_DEPTH
        self.hidden_flow_size = hidden_flow_size
        self.num_hidden_units = num_hidden_units

    def train(self, seednum=None):
        print("=== compiling ===")
        if seednum:
            seed(seednum)

        pyro.clear_param_store()
        optimiser = pyro.optim.Adam({"lr": self.lr})

        model = pcfg_gpsyn_model(
            xs=self.xs,
            MAX_DEPTH=self.MAX_DEPTH
        )
        guide = pcfg_gpsyn_guide(
            xs=self.xs,
            num_hidden_units=self.num_hidden_units,
            hidden_flow_size=self.hidden_flow_size,
            MAX_DEPTH=self.MAX_DEPTH,
        )

        capacity = count_parameters(guide)
        print(f"capacity : {capacity}")

        self.csis = pyro.infer.CSIS(
            model,
            guide,
            optimiser,
            num_inference_samples=self.num_inference_samples,
            training_batch_size=self.training_batch_size
        )

        training_loss = []
        pbar = tqdm.trange(self.n_steps)
        training_total_time = 0
        for steps in pbar:
            step_begin_time = time.time()
            loss = self.csis.step()
            step_end_time = time.time()
            training_total_time += step_end_time - step_begin_time
            training_loss.append(loss)
            last25loss = sum(training_loss[-25:]) / len(training_loss[-25:])
            pbar.set_description("R25(%g)" % (last25loss))

        print(f"training_total_time : {training_total_time} secs")
        print(f"training loss : {training_loss}")

        plot_loss(training_loss, sep=25, show=False, file_name=f"{self.name}_loss.png")

        self.train_dict = {
            "lr": self.lr,
            "nsteps": self.n_steps,
            "num_inference_samples": self.num_inference_samples,
            "training_batch_size": self.training_batch_size,
            "training_loss": training_loss,
            "capacity": capacity
        }
        print(f"capacity_wrong: {count_parameters(guide)}")


    def convert_mean_cov(self, kern, Xnew):
        mean, cov = gp.util.conditional(Xnew=Xnew,X=self.xs,kernel=kern,f_loc=self.ys,full_cov=True)
        return mean, cov

    def inference(
            self,
            xs_test=T.linspace(0, 12, 500),
            ys_test=T.linspace(0, 12, 500)
    ):
        print("=== sampling ===")
        inference_start = time.time()
        posterior = self.csis.run(observations={
                "ys": self.ys
        })
        inference_end = time.time()
        inference_total_time = inference_end - inference_start
        print(f"inference_total_time : {inference_total_time} secs")
        print(f"ESS : {posterior.get_ESS()}")

        empirical = TraceWrapper(posterior.exec_traces, posterior.log_weights)
        empirical.run()

        def sample_RETURN(empirical):
            return empirical().nodes["_RETURN"]["value"]

        print()
        print("Three samples:")
        print(visitKernel(sample_RETURN(empirical)))
        print(visitKernel(sample_RETURN(empirical)))
        print(visitKernel(sample_RETURN(empirical)))

        print("log_weights:")
        print(T.tensor(posterior.log_weights))
        print("probs:")
        print(TNF.softmax(T.tensor(posterior.log_weights),dim=0))

        with T.no_grad():
            trace = empirical()
            kern  = trace.nodes["_RETURN"]["value"]
            noise = trace.nodes["noise"]["value"]

            mean, cov = gp.util.conditional(
                Xnew=xs_test,
                X=self.xs,
                kernel=kern,
                f_loc=self.ys,
                full_cov=True,
                jitter=noise
            )

            # mean,cov = self.convert_mean_cov(kern, xs_test)
            mean, cov = gp.util.conditional(
                Xnew=xs_test,
                X=self.xs,
                kernel=kern,
                f_loc=self.ys,
                full_cov=True,
                jitter=noise
            )
            sd = cov.diag().sqrt()  # standard deviation at each input point x
            mse = TNF.mse_loss(mean, ys_test, reduction='mean')
            print(f"Test MSE : {mse}")
            m = ys_test.size(dim=0)
            mvnormal = PD.MultivariateNormal(
                loc=mean,
                covariance_matrix=cov + noise.expand(m).diag()
            )
            pll = mvnormal.log_prob(ys_test)
            print(f"Test PLL : {pll}")
            # plot_gp(xs_test,ys_test,mean,sd,f"{self.name}_predict.pg")

            xs_all = T.cat([self.xs, xs_test])
            ys_all = T.cat([self.ys, ys_test])
            # mean,cov = self.convert_mean_cov(kern, xs_all)
            mean, cov = gp.util.conditional(
                Xnew=xs_all,
                X=self.xs,
                kernel=kern,
                f_loc=self.ys,
                full_cov=True,
                jitter=noise
            )
            sd = cov.diag().sqrt()  # standard deviation at each input point x
            plot_gp(xs_all,ys_all,mean,sd,f"{self.name}_all.png")

def make_dataset_sanity():
    kernels = [
        gp.kernels.Constant(input_dim=1, variance=torch.tensor(2.7597498893737793)),
        gp.kernels.WhiteNoise(input_dim=1, variance=torch.tensor(17.782581329345703)),
        gp.kernels.Polynomial(input_dim=1, degree=1, bias=torch.tensor(38.61073684692383)),
        gp.kernels.Periodic(input_dim=1, variance=torch.tensor(1.0), period=torch.tensor(1.0016099214553833), lengthscale=torch.tensor(0.1125849261879921))
    ]
    kernel = gp.kernels.Sum(
        gp.kernels.Sum(kernels[0], kernels[1]),
        gp.kernels.Sum(kernels[2], kernels[3])
        )

    N = 144
    X = T.linspace(0.0, 10.0, N)
    # X = PD.Uniform(0.0, 10.0).sample(sample_shape=(N,))
    cov = kernel(X)
    Y = PD.MultivariateNormal(
        loc=T.zeros(N),
        covariance_matrix=cov
    ).sample()

    print(f"X : {X}")
    print(f"Y : {Y}")
    return X, Y

def make_dataset():
    # (RBF[variance=169.64540100097656, lengthscale=0.2108091115951538]) + (((Constant[2.7597498893737793]) + (WhiteNoise[17.782581329345703])) + ((Polynomial[bias=38.61073684692383, degree=1]) + (Periodic[variance=888.4461059570312, lengthscale=0.1125849261879921, period=1.0016099214553833])))
    kernels = [
        gp.kernels.RBF(input_dim=1, variance=torch.tensor(169.64540100097656), lengthscale=torch.tensor(0.2108091115951538)),
        gp.kernels.Constant(input_dim=1, variance=torch.tensor(2.7597498893737793)),
        gp.kernels.WhiteNoise(input_dim=1, variance=torch.tensor(17.782581329345703)),
        gp.kernels.Polynomial(input_dim=1, degree=1, bias=torch.tensor(38.61073684692383)),
        gp.kernels.Periodic(input_dim=1, variance=torch.tensor(888.4461059570312), period=torch.tensor(1.0016099214553833), lengthscale=torch.tensor(0.1125849261879921))
    ]
    kernel = gp.kernels.Sum(
        kernels[0],
        gp.kernels.Sum(
            gp.kernels.Sum(kernels[1], kernels[2]),
            gp.kernels.Sum(kernels[3], kernels[4])
        ),
    )
    N = 144
    X = T.linspace(0.0, 10.0, N)
    # X = PD.Uniform(0.0, 10.0).sample(sample_shape=(N,))
    cov = kernel(X)
    Y = PD.MultivariateNormal(
        loc=T.zeros(N),
        covariance_matrix=cov
    ).sample()

    print(f"X : {X}")
    print(f"Y : {Y}")
    return X, Y

def main():
    seed(0)

    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--MAX_DEPTH", type=int, required=True)
    parser.add_argument("--lr", type=float, required=True)
    parser.add_argument("--hidden_flow_size", type=int, required=True)
    parser.add_argument("--num_hidden_units", type=int, required=True)
    parser.add_argument("--num_inference_samples", type=int, required=True)
    parser.add_argument("--training_batch_size", type=int, required=True)
    parser.add_argument("--n_steps", type=int, required=True)
    parser.add_argument("--smoke_testing", type=bool, required=False)

    args = parser.parse_args()

    if args.smoke_testing:
        return smoke_testing()

    # xs, ys = read_airlines("./airline.csv")
    xs, ys = make_dataset_sanity()
    xs_train = xs[0:100]
    ys_train = ys[0:100]
    xs_test = xs[100:]
    ys_test = ys[100:]

    # xs_train = xs
    # ys_train = ys
    # xs_test = xs
    # ys_test = ys

    dir = datetime.now().strftime('dir_%d_%m_%Y_%H_%M')
    Path(f"./{dir}").mkdir(parents=True, exist_ok=True)

    solver = synthesizer(
        name=f"./{dir}/handcraft",
        xs=xs_train,
        ys=ys_train,
        MAX_DEPTH=args.MAX_DEPTH,
        lr=args.lr,
        hidden_flow_size=args.hidden_flow_size,
        num_hidden_units=args.num_hidden_units,
        num_inference_samples=args.num_inference_samples,
        training_batch_size=args.training_batch_size,
        n_steps=args.n_steps
    )
    solver.train()
    f = open(f"result_airline.json", "w")
    json.dump(solver.train_dict, f, indent=4, sort_keys=True)
    solver.inference(xs_test,ys_test)

def smoke_testing():
    xs, ys = read_airlines("./airline.csv")
    xs_train = xs[0:100]
    ys_train = ys[0:100]
    xs_test = xs[100:]
    ys_test = ys[100:]

    # xs_train = xs
    # ys_train = ys
    # xs_test = xs
    # ys_test = ys

    solver = synthesizer(
        name="airline",
        xs=xs_train,
        ys=ys_train,
        MAX_DEPTH=3,
        lr=5e-3,
        hidden_flow_size=35,
        num_hidden_units=35,
        num_inference_samples=100,
        n_steps=10
    )
    for i in range(11,100):
        print(f"seed {i}")
        solver.train(seednum=None)
        f = open(f"result_airline.json", "w")
        json.dump(solver.train_dict, f, indent=4, sort_keys=True)
        solver.inference(xs_test,ys_test)

if __name__ == '__main__':
    main()