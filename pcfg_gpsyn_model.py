import pyro as P
import pyro.contrib.gp as gp
import pyro.distributions as PD
import torch as T
import pyro.nn as PN

from pcfg_gpsyn_utils import visitKernel

class pcfg_gpsyn_model(PN.PyroModule):
    def __init__(
            self,
            MAX_DEPTH=5,
            xs=T.tensor(list(range(10)))
    ):
        assert len(xs.size()) == 1
        super().__init__(name="pcfg_gpsyn_model")
        self.xs = xs
        self.n = xs.size(dim=0)
        self.MAX_DEPTH = MAX_DEPTH

    # Gaussian Process PCFG
    def model_GPkernel(self, prefix="", depth=None):
        composite = P.sample(f"{prefix}_compo", PD.Bernoulli(.6))
        composite = int(composite)
        if composite == 1 and depth < self.MAX_DEPTH:
            ind = P.sample(f"{prefix}_compoind", PD.Categorical(logits=T.tensor([1., 1.])))
            if ind == 0:
                kernel1 = self.model_GPkernel(f"{prefix}_sum1", depth + 1)
                kernel2 = self.model_GPkernel(f"{prefix}_sum2", depth + 1)
                return gp.kernels.Sum(kernel1, kernel2)
            elif ind == 1:
                kernel1 = self.model_GPkernel(f"{prefix}_prod1", depth + 1)
                kernel2 = self.model_GPkernel(f"{prefix}_prod2", depth + 1)
                return gp.kernels.Product(kernel1, kernel2)
        else:
            ind = P.sample(f"{prefix}_primind", PD.Categorical(logits=T.tensor([1., 1., 1., 1., 1.])))
            if ind == 0:
                const_v = P.sample(f"{prefix}_const_v", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                return gp.kernels.Constant(input_dim=1, variance=const_v)
            elif ind == 1:
                wn_v = P.sample(f"{prefix}_wn_v", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                return gp.kernels.WhiteNoise(input_dim=1, variance=wn_v)
            elif ind == 2:
                linear_b = P.sample(f"{prefix}_linear_b", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                return gp.kernels.Polynomial(input_dim=1, degree=1, bias=linear_b)
            elif ind == 3:
                periodic_p = P.sample(f"{prefix}_periodic_p", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                periodic_l = P.sample(f"{prefix}_periodic_l", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                return gp.kernels.Periodic(input_dim=1, period=periodic_p, lengthscale=periodic_l)
            elif ind == 4:
                rbf_l = P.sample(f"{prefix}_rbf_l", PD.Gamma(T.tensor(1.), T.tensor(2.)))
                return gp.kernels.RBF(input_dim=1, lengthscale=rbf_l)

    def model(
            self,
            observations={"ys": T.tensor([])}
    ):

        n = self.n
        self.kernel = self.model_GPkernel(depth=0)
        self.kernel.set_mode("model")

        noise = P.sample("noise", PD.Gamma(T.tensor(1.), T.tensor(1.)))
        before = visitKernel(self.kernel)
        cov_mat = self.kernel(self.xs) + noise.expand(n).diag()
        after  = visitKernel(self.kernel)
        assert before == after, f"\nbefore : {before}\nafter  : {after}\n"
        # print(before)
        P.sample(
            "ys",
            PD.MultivariateNormal(
                loc=T.zeros(n),
                covariance_matrix=cov_mat
            ),
            obs=observations["ys"]
        )

        return self.kernel

    def __call__(self, *args, **kwargs):
        return self.model(*args, **kwargs)


if __name__ == '__main__':
    # unconditioned = P.poutine.uncondition(pcfg_gpsyn_model())
    # unconditioned()
    model = pcfg_gpsyn_model()
    model()
    model()
    model()
    model()
    model()