import pyro as P
import torch
import torch as T
import pyro.distributions as PD
import pyro.contrib.gp as gp
import torch.nn as TN
import pyro.nn as PN
import torch.nn.functional as TNF
import pyro.nn as PN

from pcfg_gpsyn_utils import visitKernel

class pcfg_gpsyn_guide(PN.PyroModule):
    def __init__(
            self,
            MAX_DEPTH = 5,
            xs=T.tensor(list(range(10))),
            hidden_flow_size = 35,
            num_hidden_units = 35,
            epsilon = 1
    ):
        assert len(xs.size()) == 1
        super().__init__(name="pcfg_gpsyn_guide")

        self.MAX_DEPTH = MAX_DEPTH
        self.xs = xs
        self.n = xs.size(dim=0)

        self.hidden_flow_size = hidden_flow_size,
        self.num_hidden_units = num_hidden_units,
        self.epsilon = epsilon

        self.nn_noise = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )

        self.nn_hidden = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + 1, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, hidden_flow_size)
        )

        self.nn_composite =  PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 1)
        )

        self.nn_then_ind =  PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_else_ind =  PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 5)
        )
        self.nn_hidden_update = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](hidden_flow_size + 2, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, hidden_flow_size)
        )

        self.nn_merge_lhs = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](num_hidden_units + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, hidden_flow_size)
        )

        self.nn_convertion_embedding = PN.PyroModule[TN.GRU](
            input_size=7+2,
            hidden_size=num_hidden_units,
            num_layers=1
        )

        self.nn_const_v = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_wn_v = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_linear_b = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_periodic_p = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_periodic_l = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )
        self.nn_rbf_l = PN.PyroModule[TN.Sequential](
            PN.PyroModule[TN.Linear](self.n + hidden_flow_size, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            # PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            # PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, num_hidden_units),
            PN.PyroModule[TN.ReLU](),
            PN.PyroModule[TN.Linear](num_hidden_units, 2)
        )

    def get_sentence(self, k):
        num_cat = torch.tensor(7)
        if type(k) == gp.kernels.Sum:
            plus_embed =  T.cat([TNF.one_hot(T.tensor(0), num_cat), torch.zeros(2)]).unsqueeze(0)
            return T.cat([self.get_sentence(k.kern0), plus_embed, self.get_sentence(k.kern1)])
        elif type(k) == gp.kernels.Product:
            prod_embed =  T.cat([TNF.one_hot(T.tensor(1), num_cat), torch.zeros(2)]).unsqueeze(0)
            return T.cat([self.get_sentence(k.kern0), prod_embed, self.get_sentence(k.kern1)])
        elif type(k) == gp.kernels.Constant:
            return T.cat([TNF.one_hot(T.tensor(2), num_cat), k.variance.unsqueeze(0), torch.zeros(1)]).unsqueeze(0)
        elif type(k) == gp.kernels.WhiteNoise:
            return T.cat([TNF.one_hot(T.tensor(3), num_cat), k.variance.unsqueeze(0), torch.zeros(1)]).unsqueeze(0)
        elif type(k) == gp.kernels.Polynomial:
            return T.cat([TNF.one_hot(T.tensor(4), num_cat), k.variance.unsqueeze(0), torch.zeros(1)]).unsqueeze(0)
        elif type(k) == gp.kernels.Periodic:
            return T.cat([TNF.one_hot(T.tensor(5), num_cat), k.lengthscale.unsqueeze(0), k.period.unsqueeze(0)]).unsqueeze(0)
        elif type(k) == gp.kernels.RBF:
            return T.cat([TNF.one_hot(T.tensor(6), num_cat), k.lengthscale.unsqueeze(0), torch.zeros(1)]).unsqueeze(0)

    def embed_kern(self, k):
        sentence = self.get_sentence(k).unsqueeze(1)
        emb, _ = self.nn_convertion_embedding (sentence)
        return emb[-1][0]

    def greater_than_epsilon(self, x):
        return self.epsilon + TNF.softplus(x)

    # Gaussian Process PCFG
    def guide_GPkernel(self, _obs, _hidden, prefix = "", depth = None):
        # P.module('nn_composite',self.nn_composite)
        # P.module('nn_then_ind',self.nn_then_ind)
        # P.module('nn_else_ind',self.nn_else_ind)
        # P.module('nn_hidden_update',self.nn_hidden_update)
        # P.module('nn_const_v',self.nn_const_v)
        # P.module('nn_wn_v',self.nn_wn_v)
        # P.module('nn_linear_b',self.nn_linear_b)
        # P.module('nn_periodic_p',self.nn_periodic_p)
        # P.module('nn_periodic_l',self.nn_periodic_l)
        # P.module('nn_rbf_l',self.nn_rbf_l)

        obs_and_hidden = T.cat([_obs, _hidden], -1)

        composite_params = self.nn_composite(obs_and_hidden)
        composite = P.sample(f"{prefix}_compo", PD.Bernoulli(T.sigmoid(composite_params[0])))
        if composite == 1 and depth < self.MAX_DEPTH:
            ind_params = self.nn_then_ind(obs_and_hidden)
            ind = P.sample(f"{prefix}_compoind", PD.Categorical(logits=ind_params))
            new_hidden = self.nn_hidden_update(T.cat([_hidden, TNF.one_hot(ind,2)],-1))
            if ind == 0:
                kernel1 = self.guide_GPkernel(_obs, new_hidden, f"{prefix}_sum1", depth + 1)
                new_new_hiden = self.nn_merge_lhs(T.cat([self.embed_kern(kernel1), new_hidden]))
                kernel2 = self.guide_GPkernel(_obs, new_new_hiden, f"{prefix}_sum2", depth + 1)
                return gp.kernels.Sum(kernel1, kernel2)
            elif ind == 1:
                kernel1 = self.guide_GPkernel(_obs, new_hidden, f"{prefix}_prod1", depth + 1)
                new_new_hiden = self.nn_merge_lhs(T.cat([self.embed_kern(kernel1), new_hidden]))
                kernel2 = self.guide_GPkernel(_obs, new_new_hiden, f"{prefix}_prod2", depth + 1)
                return gp.kernels.Product(kernel1, kernel2)
        else:
            ind_params = self.nn_else_ind(obs_and_hidden)
            ind = P.sample(f"{prefix}_primind", PD.Categorical(logits=ind_params))
            if ind == 0:
                const_v_params = self.nn_const_v(obs_and_hidden)
                const_v = P.sample(f"{prefix}_const_v", PD.Gamma(self.greater_than_epsilon(const_v_params[0]), self.greater_than_epsilon(const_v_params[1])))
                return gp.kernels.Constant(input_dim=1, variance=const_v)
            elif ind == 1:
                wn_v_params = self.nn_wn_v(obs_and_hidden)
                wn_v = P.sample(f"{prefix}_wn_v", PD.Gamma(self.greater_than_epsilon(wn_v_params[0]), self.greater_than_epsilon(wn_v_params[1])))
                return gp.kernels.WhiteNoise(input_dim=1, variance=wn_v)
            elif ind == 2:
                linear_b_params = self.nn_linear_b(obs_and_hidden)
                linear_b = P.sample(f"{prefix}_linear_b", PD.Gamma(self.greater_than_epsilon(linear_b_params[0]), self.greater_than_epsilon(linear_b_params[1])))
                return gp.kernels.Polynomial(input_dim=1, degree=1, bias=linear_b)
            elif ind == 3:
                periodic_p_params = self.nn_periodic_p(obs_and_hidden)
                periodic_p = P.sample(f"{prefix}_periodic_p", PD.Gamma(self.greater_than_epsilon(periodic_p_params[0]), self.greater_than_epsilon(periodic_p_params[1])))
                periodic_l_params = self.nn_periodic_l(obs_and_hidden)
                periodic_l = P.sample(f"{prefix}_periodic_l", PD.Gamma(self.greater_than_epsilon(periodic_l_params[0]), self.greater_than_epsilon(periodic_l_params[1])))
                return gp.kernels.Periodic(input_dim=1, period=periodic_p, lengthscale=periodic_l)
            elif ind == 4:
                rbf_l_params = self.nn_rbf_l(obs_and_hidden)
                rbf_l = P.sample(f"{prefix}_rbf_l", PD.Gamma(self.greater_than_epsilon(rbf_l_params[0]), self.greater_than_epsilon(rbf_l_params[1])))
                k = gp.kernels.RBF(input_dim=1, lengthscale=rbf_l)
                assert k.variance.item() == 1.0
                return k

    def guide(
            self,
            observations={"ys":  T.tensor([])}
    ):
        # P.module('nn_hidden', self.nn_hidden)
        # P.module('nn_noise', self.nn_noise)

        n = self.n
        ys = observations["ys"]

        noise_params = self.nn_noise(ys)
        noise =  P.sample("noise", PD.Gamma(self.greater_than_epsilon(noise_params[0]), self.greater_than_epsilon(noise_params[1])))

        _obs    = ys
        _hidden = self.nn_hidden(torch.cat([_obs, noise.unsqueeze(0)]))

        self.kernel = self.guide_GPkernel(_obs,_hidden, depth=0)
        self.kernel.set_mode("model")
        # print(visitKernel(self.kernel))

        cov_mat = self.kernel(self.xs) + noise.expand(n).diag()
        sanity = PD.MultivariateNormal(
            loc=T.zeros(n),
            covariance_matrix=cov_mat
        )

    def __call__(self, *args, **kwargs):
        return self.guide(*args,**kwargs)
