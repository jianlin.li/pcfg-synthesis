import copy
import os
import matplotlib.pyplot as plt
import torch
import numpy as np

import copy
import pyro
import pyro.contrib.gp as gp
import pyro.distributions as dist

from pcfg_gpsyn_utils import *
from matplotlib.animation import FuncAnimation
from mpl_toolkits.axes_grid1 import make_axes_locatable

# import seaborn as sns
# from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay

smoke_test = "CI" in os.environ  # ignore; used to check code integrity in the Pyro repo
# assert pyro.__version__.startswith('1.8.1')
pyro.set_rng_seed(0)
pyro.clear_param_store()

xs, ys = read_airlines("./airline.csv")
xs_train = xs[0:100]
ys_train = ys[0:100]
xs_test = xs[100:]
ys_test = ys[100:]

N = xs.size(dim=0)

kernels = [
    gp.kernels.RBF(input_dim=1, variance=torch.tensor(6.0), lengthscale=torch.tensor(0.05)),
    gp.kernels.Constant(input_dim=1, variance=torch.tensor(1.0)),
    gp.kernels.WhiteNoise(input_dim=1, variance=torch.tensor(1.0)),
    gp.kernels.Polynomial(input_dim=1, degree=1, bias=torch.tensor(1.0)),
    gp.kernels.Periodic(input_dim=1, period=torch.tensor(1.0), lengthscale=torch.tensor(1.0))
]

kernel = gp.kernels.Sum(
    kernels[0],
    gp.kernels.Sum(
        gp.kernels.Sum(kernels[1], kernels[2]),
        gp.kernels.Sum(kernels[3], kernels[4])
    )
)

gpr = gp.models.GPRegression(xs_train, ys_train, kernel, noise=torch.tensor(0.1))
print(visitKernel(gpr.kernel))

optimizer = torch.optim.Adam(gpr.parameters(), lr=0.005)
loss_fn = pyro.infer.Trace_ELBO().differentiable_loss
losses = []
variances = []
lengthscales = []
noises = []
num_steps = 20000 if not smoke_test else 2

pbar = tqdm.trange(num_steps)
# for i in pbar:
#     # variances.append(gpr.kernel.variance.item())
#     noises.append(gpr.noise.item())
#     # lengthscales.append(gpr.kernel.lengthscale.item())
#     optimizer.zero_grad()
#     loss = loss_fn(gpr.model, gpr.guide)
#     kernels.append(copy.deepcopy(gpr.model))
#     loss.backward()
#     optimizer.step()
#     losses.append(loss.item())

fig, ax = plt.subplots(figsize=(12, 6))

def nop():
    pass

def update(iteration):
    for i in range(50):
        pbar.update()
        noises.append(gpr.noise.item())
        # variances.append(gpr.kernel.variance.item())
        # lengthscales.append(gpr.kernel.lengthscale.item())
        optimizer.zero_grad()
        loss = loss_fn(gpr.model, gpr.guide)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
        with torch.no_grad():
            cov = gpr.kernel(xs)
    ax.cla()
    # kernel_iter = gp.kernels.RBF(
    #     input_dim=1,
    #     variance=torch.tensor(variances[iteration]),
    #     lengthscale=torch.tensor(lengthscales[iteration]),
    # )
    with torch.no_grad():
        mean, cov = gpr(xs, full_cov=True, noiseless=False)
        # mean, cov = gp.util.conditional(Xnew=xs,X=xs_train,kernel=kernel_iter,f_loc=ys_train,full_cov=True)

        sd = cov.diag().sqrt()  # standard deviation at each input point x
        ax.plot(xs.numpy(), ys.numpy(), "kx")
        ax.plot(xs.numpy(), mean.numpy(), "r", lw=2)  # plot the mean
        ax.fill_between(
            xs.numpy(),  # plot the two-sigma uncertainty about the mean
            (mean - 2.0 * sd).numpy(),
            (mean + 2.0 * sd).numpy(),
            color="C0",
            alpha=0.3,
        )
        ax.set_title(f"Iteration: {iteration}, Loss: {losses[iteration]:0.2f}")


anim = FuncAnimation(fig, update, frames=range(0, num_steps, 50), interval=100, init_func=nop)
plt.close()
anim.save("gpr-fit.gif", fps=120)

pbar.close()
print(visitKernel(gpr.kernel))

plot_loss(losses, file_name=f"airline_trace_elbo_loss.png")

print(f"variances[:10]    \t: {variances[:10]}")
print(f"lengthscales[:10] \t: {lengthscales[:10]}")
print(f"noises[:10]       \t: {noises[:10]}")
print(f"losses[:10]       \t: {losses[:10]}")

print(f"variances[-10:]    \t: {variances[-10:]}")
print(f"lengthscales[-10:] \t: {lengthscales[-10:]}")
print(f"noises[-10:]       \t: {noises[-10:]}")
print(f"losses[-10:]       \t: {losses[-10:]}")