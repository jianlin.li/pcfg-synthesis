import torch
import csv
import tqdm
import torch as T
import json
import pyro
import random
import os

import pyro.nn as PN
import pyro.contrib.gp as gp
import matplotlib.pyplot as plt
from TraceWrapper import *

def seed(seednum = 1):
    pyro.set_rng_seed(seednum)
    random.seed(seednum)
    torch.manual_seed(seednum)
    torch.cuda.manual_seed(seednum)
    os.environ['PYTHONHASHSEED'] = str(seednum)
    torch.backends.cudnn.deterministic = True

def read_airlines(path="airline.csv"):
    with open(path) as f:
        ts = []
        vs = []
        for t, v in csv.reader(f):
            ts.append(float(t))
            vs.append(float(v))
    return torch.tensor(ts, requires_grad=False), torch.tensor(vs, requires_grad=False)

def visitKernel(k):
    if type(k) == gp.kernels.Sum:
        return f"({visitKernel(k.kern0)}) + ({visitKernel(k.kern1)})"
    elif type(k) == gp.kernels.Product:
        return (f"({visitKernel(k.kern0)}) * ({visitKernel(k.kern1)})")
    elif type(k) == gp.kernels.Constant:
        return (f"Constant[{k.variance.item()}]")
    elif type(k) == gp.kernels.WhiteNoise:
        return (f"WhiteNoise[{k.variance.item()}]")
    elif type(k) == gp.kernels.Polynomial:
        return (f"Polynomial[bias={k.bias.item()}, degree={k.degree}]")
    elif type(k) == gp.kernels.Periodic:
        return (f"Periodic[variance={k.variance.item()}, lengthscale={k.lengthscale.item()}, period={k.period.item()}]")
    elif type(k) == gp.kernels.RBF:
        return (f"RBF[variance={k.variance.item()}, lengthscale={k.lengthscale.item()}]")

def count_all_parameters(o):
    res = 0
    for feild in vars(o).values():
        if isinstance(feild, PN.PyroModule):
            res += count_parameters(feild)
    return res

def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

def plot_loss(loss, show=False, sep=None, file_name=None):
    fig, ax = plt.subplots(figsize=(12, 6))
    if sep:
        avg_loss = [ sum(loss[:i+1][-sep:])/len(loss[:i+1][-sep:]) for i in range(len(loss)) ]
        loss = avg_loss
    ax.plot(loss)
    plt.xlabel("Iterations")
    _ = plt.ylabel("Loss")  # supress output text
    if file_name is not None:
        plt.savefig(file_name)
    if show:
        plt.show()

def plot_gp(xs,ys,mean,sd,name, show=False):
    fig, ax = plt.subplots(figsize=(12, 6))
    # ax.plot(self.xs.numpy(), self.ys.numpy(), "kx_train")
    ax.plot(xs.numpy(), ys.numpy(), "kx")
    ax.plot(xs.numpy(), mean.numpy(), "r", lw=2)  # plot the mean
    ax.fill_between(
        xs.numpy(),  # plot the two-sigma uncertainty about the mean
        (mean - 2.0 * sd).numpy(),
        (mean + 2.0 * sd).numpy(),
        color="C0",
        alpha=0.3,
    )
    plt.savefig(f"{name}")
    if show:
        plt.show()
