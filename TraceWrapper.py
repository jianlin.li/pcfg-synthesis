from pyro.infer.abstract_infer import TracePosterior
import torch

# Create a wrapper for traces to create EmpiricalMarginal
class TraceWrapper(TracePosterior):
  def __init__(self, traces, weights):
    self.tr = traces
    self.lw = weights
    self.trace_count = len(self.tr)
    super().__init__()

  def _traces(self, *args, **kwargs):
      yield from zip( self.tr, self.lw)

  def get_normalized_weights(self, log_scale=False):
    """
    Compute the normalized importance weights.
    """
    if self.lw:
        log_w = torch.tensor(self.lw)
        log_w_norm = log_w - torch.logsumexp(log_w, 0)
        return log_w_norm if log_scale else torch.exp(log_w_norm)

  def get_ESS(self):
    """
    Compute (Importance Sampling) Effective Sample Size (ESS).
    """
    if self.lw:
        log_w_norm = self.get_normalized_weights(log_scale=True)
        ess = torch.exp(-torch.logsumexp(2 * log_w_norm, 0))
    else:
        ess = 0
    return ess