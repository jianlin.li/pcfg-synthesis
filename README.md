```
conda env create -f environment.yml

[$] <git:(master*)> python synthesizer.py --MAX_DEPTH 4 --lr 0.005 --hidden_flow_size 35 --num_hidden_units 35 --training_batch_size=32 --num_inference_samples 100 --n_steps 10

```

